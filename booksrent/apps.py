from django.apps import AppConfig


class BooksrentConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'booksrent'
