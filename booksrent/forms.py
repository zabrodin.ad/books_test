from .models import Books
from django.forms import ModelForm, TextInput, Select, CheckboxInput, Textarea


class BooksForm(ModelForm):
    class Meta:
        model = Books
        fields = ('shelf', 'title', 'author', 'short_description', 'status',)

        widgets = {
            "shelf": Select(attrs={
                'class': 'form-control',
            }),
            "title": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Название книги',
            }),
            "author": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Автор книги',
            }),
            "short_description": Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Краткое содержание книги',
            }),
        }