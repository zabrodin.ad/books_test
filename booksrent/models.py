from django.db import models

class Shelfs(models.Model):
    shelf_title = models.CharField('Название стеллажа', max_length=100)

    def __str__(self):
        return self.shelf_title

    class Meta:
        verbose_name = 'Стеллаж'
        verbose_name_plural = 'Стеллажи'

class Books(models.Model):
    shelf = models.ForeignKey(Shelfs, on_delete=models.CASCADE)
    title = models.CharField('Название', max_length=100)
    author = models.CharField('Автор', max_length=100)
    short_description = models.TextField('Краткое описание', max_length=2000)
    status = models.BooleanField('Доступна', default=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return f'/books/{self.id}'

    class Meta:
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'



