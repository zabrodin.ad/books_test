from . import views
from django.urls import path
from .views import *


urlpatterns = [
    path('', views.books_rent, name='books'),
    path('api/', Book.as_view()),
    path('<int:pk>', views.BooksDetailView.as_view(), name='books_detail'),
    path('<int:pk>/update', views.BooksUpdateView.as_view(), name='books_update'),
    path('<int:pk>/delete', views.BooksDeleteView.as_view(), name='books_delete'),
    path('form', views.create, name='create')
]