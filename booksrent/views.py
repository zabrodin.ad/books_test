from django.shortcuts import render, redirect
from rest_framework.views import APIView
from rest_framework.response import Response
from django.views.generic import DetailView, UpdateView, DeleteView
from .forms import BooksForm
from rest_framework import authentication, permissions
from django.contrib.auth.models import User

from .models import Shelfs, Books
from .serializers import ShelfsSerializer, BooksSerializer

class Shelf(APIView):
    """Стеллажи"""

    def get(self, request):
        shelfs = Shelfs.objects.all()
        serializer = ShelfsSerializer(shelfs, many=True)
        return Response({"data": serializer.data})

class Book(APIView):
    """Стеллажи"""

    def get(self, request):
        books = Books.objects.all()
        serializer = BooksSerializer(books, many=True)
        return Response({"data": serializer.data})

class BooksDetailView(DetailView):
    model = Books
    template_name = 'booksrent/books_detail.html'
    context_object_name = 'book'

class BooksUpdateView(UpdateView):
    model = Books
    template_name = 'booksrent/book_form.html'
    form_class = BooksForm

class BooksDeleteView(DeleteView):
    model = Books
    success_url = '/books/'
    template_name = 'booksrent/book_delete.html'


def books_rent(request):
    books = Books.objects.order_by('-shelf')
    serializer = BooksSerializer(books, many=True)
    shelfs = Shelfs.objects.all()
    return render(request, 'booksrent/index.html', {'books': serializer.data, 'shelfs': shelfs})

def create(request):
    error = ''
    if request.method == 'POST':
        form = BooksForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('books')
        else:
            error = 'Вы неправильно заполнили форму'

    form = BooksForm()

    data = {
        'form': form,
        'error': error,
    }
    return render(request, 'booksrent/book_form.html', data)
