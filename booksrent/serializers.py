from rest_framework import serializers

from booksrent.models import Books, Shelfs

class BooksSerializer(serializers.ModelSerializer):
    """Сериализатор книг"""

    class Meta:
        model = Books
        fields = ('shelf', 'title', 'author', 'short_description', 'status', 'id',)

class ShelfsSerializer(serializers.ModelSerializer):
    """Сериализатор стеллажей"""

    #books = BooksSerializer()
    class Meta:
        model = Shelfs
        fields = ('shelf_title')
